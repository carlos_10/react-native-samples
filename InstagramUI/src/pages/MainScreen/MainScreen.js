import {Platform, StyleSheet, Text, View} from 'react-native';
import React, {Component} from 'react';

import AddMediaTab from '../../../Components/AppTabNavigator/AddMediaTab';
import HomeTab from '../../../Components/AppTabNavigator/HomeTab';
import {Icon} from 'native-base';
import LikesTab from '../../../Components/AppTabNavigator/LikesTab';
import ProfileTab from '../../../Components/AppTabNavigator/ProfileTab';
import SearchTab from '../../../Components/AppTabNavigator/SearchTab';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const MainScreen = () => {
  return <AppTabNavigator />;
};

const Tab = createBottomTabNavigator();

const AppTabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeTab"
      tabBarOptions={{
        activeTintColor: '#000',
        inactiveTintColor: '#d1cece',
        showLabel: false,
        style: styles.platformTab,
      }}>
      <Tab.Screen
        name="HomeTab"
        component={HomeTab}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="home-outline" style={{color: color}} />
          ),
        }}
      />
      <Tab.Screen
        name="SearchTab"
        component={SearchTab}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="search-outline" style={{color: color}} />
          ),
        }}
      />
      <Tab.Screen
        name="AddMediaTab"
        component={AddMediaTab}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="add-circle-outline" style={{color: color}} />
          ),
        }}
      />
      <Tab.Screen
        name="LikesTab"
        component={LikesTab}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="heart-outline" style={{color: color}} />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileTab"
        component={ProfileTab}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="person-outline" style={{color: color}} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  platformTab: {
    ...Platform.select({
      android: {
        backgroundColor: 'white',
      },
      ios: {
        backgroundColor: 'white',
      },
    }),
  },
});

export default MainScreen;
