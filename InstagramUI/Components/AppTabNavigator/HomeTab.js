import {Body, Container, Content, Header, Icon, Left, Right, Thumbnail} from 'native-base';
import {ScrollView, StyleSheet, Text, View} from 'react-native';

import CardComponent from '../CardComponent/CardComponent';
import React from 'react';

const HomeTab = () => {
  return (
    <Container style={styles.container}>
      <Header >
        <Left><Icon name="camera-outline" style={{paddingLeft: 10}} /></Left>
        <Body>
          <Text>Instagram</Text>
        </Body>
        <Right><Icon name="send-outline" style={{paddingRight: 10}} /></Right>
      </Header>
      <Content>
        <View style={{height: 100}}>
          <View style={[styles.headerCont]}>
            <Text style={{fontWeight: '700'}}>Stories</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Icon
                style={{
                  fontSize: 14,
                }}
                name="md-play"
              />
              <Text>Watch All</Text>
            </View>
          </View>
          <View style={{flex: 3}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.containerHeaderStyle}>
              <Thumbnail
                style={styles.storieThumb}
                source={require('../../assets/img/stories/ex.jpg')}
              />
              <Thumbnail
                style={styles.storieThumb}
                source={require('../../assets/img/stories/ex.jpg')}
              />
              <Thumbnail
                style={styles.storieThumb}
                source={require('../../assets/img/stories/ex.jpg')}
              />
            </ScrollView>
          </View>
        </View>
        <CardComponent imageSource="1" likes="101" />
        <CardComponent imageSource="2" likes="201" />
        <CardComponent imageSource="3" likes="301" />
      </Content>
    </Container>
  );
};

export default HomeTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerCont: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  storieThumb: {
    marginHorizontal: 5,
    borderColor: 'pink',
    borderWidth: 2,
  },
  containerHeaderStyle: {
    alignItems: 'center',
    paddingStart: 5,
    paddingEnd: 5,
  },
});
