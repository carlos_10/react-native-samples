import { StyleSheet, Text, View } from 'react-native'

import React from 'react'

const AddMediaTab = () => {
    return (
        <View style={styles.container}>
            <Text>AddMediaTab</Text>
        </View>
    )
}

export default AddMediaTab

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    
})
