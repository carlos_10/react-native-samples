import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Icon,
  Left,
  Right,
} from 'native-base';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import React,{useState} from 'react';

import CardComponent from '../CardComponent/CardComponent';
import IconProfileButton from './ProfileComponents/IconButtonMidNav';
import RenderSectionOne from './ProfileComponents/RenderSectionOne';
import StatsProfile from './ProfileComponents/StatsProfile';

const ProfileTab = () => {
  const [count, setCount] = useState(0);
  segmentClicked = (index) => {
    setCount(index)
  }
  
  renderSection = ()=> {
    switch (count) {
      case 0:
        return(
          <View style={{flexDirection:'row', flexWrap:'wrap'}}>
            <RenderSectionOne />          
          </View>
        )
      case 1:
        return (
          <View>
            <CardComponent imageSource='1' likes='200'/>
            <CardComponent imageSource='2' likes='240'/>
            <CardComponent imageSource='3' likes='130'/>
          </View>
        )
        break;
    
      default:
        return(
          <View style={{flexDirection:'row', flexWrap:'wrap'}}>
            <Text>Trabajando en ello</Text>          
          </View>
        )
        break;
    }
  }
  
  return (
    <Container style={styles.container}>
      <Header>
        <Left>
          <Icon name="person-add" style={{paddingLeft: 10}} />
        </Left>
        <Body>
          <Text>im_cparedes10</Text>
        </Body>
        <Right>
          <Icon name="timer-outline" style={{paddingRight: 10}} />
        </Right>
      </Header>
      <Content>
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, alignItems:'center'}}>
              <Image
                source={require('../../assets/img/me.jpg')}
                style={styles.profilePic}
              />
            </View>
            <View style={{flex: 3, marginTop:5}}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <StatsProfile number={20} text={'posts'} />
                <StatsProfile number={205} text={'followers'} />
                <StatsProfile number={129} text={'following'} />
              </View>
              <View style={{flexDirection: 'row', marginTop:5}}>
                <Button bordered dark style={styles.borderEdit}>
                  <Text>Edit Profile</Text>
                </Button>
                <Button bordered dark style={styles.borderEdit2}>
                  <Icon
                    name="cog-outline"
                    style={{fontSize: 21, textAlign: 'center'}}
                  />
                </Button>
              </View>
            </View>
          </View>
          <View style={{padding:10}}>
            <Text style={{fontWeight:'bold'}}>Carlos Paredes </Text>
            <Text>SV | Programmer</Text>
            <Text>cparedes.devs.com</Text>
          </View>          
        </View>
        <View>
          <View style={styles.navBarMid}>            
            <IconProfileButton 
            onPress={() => segmentClicked(0)}
            active={count == 0}            
            style={[count == 0 ? {} : {color:'grey'}]}
            name={'apps-outline'} />
            
            <IconProfileButton 
            onPress={() => segmentClicked(1)}
            active={count == 1}            
            style={[count == 1 ? {} : {color:'grey'}]}
            name={'list-outline'} />
            
            <IconProfileButton 
            onPress={() => segmentClicked(2)}
            active={count == 2}            
            style={[count == 2 ? {} : {color:'grey'}]}
            name={'people-outline'} />
            
            <IconProfileButton 
            onPress={() => segmentClicked(3)}
            active={count == 3}            
            style={[count == 3 ? {} : {color:'grey'}]}
            name={'bookmark-outline'} />
            
          </View>
          {renderSection()}
        </View>
      </Content>
    </Container>
  );
};

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profilePic: {
    width: 85,
    height: 85,
    borderRadius: 37.5,
  },
  borderEdit: {
    flex: 3,
    marginTop: 5,
    marginLeft: 10,
    justifyContent: 'center',
    height: 35,
  },
  borderEdit2: {
    marginTop: 5,
    flex: 1,
    marginRight: 10,
    marginLeft: 5,
    justifyContent: 'center',
    height: 35,
  },
  navBarMid: {
    flexDirection:'row', 
    justifyContent:'space-around',
    borderTopWidth:1,
    borderTopColor:'#eae5e5',

  }
});
