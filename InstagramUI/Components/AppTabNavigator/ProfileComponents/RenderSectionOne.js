import {Dimensions, Image, StyleSheet, View} from 'react-native';

import React from 'react';

var images = [
  require('../../../assets/img/profile/1.jpg'),
  require('../../../assets/img/profile/2.jpg'),
  require('../../../assets/img/profile/3.jpg'),
  require('../../../assets/img/profile/4.jpg'),
  require('../../../assets/img/profile/5.jpg'),
  require('../../../assets/img/profile/6.jpg'),
  require('../../../assets/img/profile/7.jpg'),
  require('../../../assets/img/profile/8.png'),
  require('../../../assets/img/profile/9.jpg'),
  require('../../../assets/img/profile/10.jpg'),
  require('../../../assets/img/profile/11.png'),
  require('../../../assets/img/profile/12.png'),
];
var {width, height} = Dimensions.get('window');
const RenderSectionOne = () => {
  return images.map((image, index) => {
    return (
      <View key={index} style={[{width: width / 3}, {height: width / 3}, {marginBottom:2}, 
      index %3 !== 0 ? {paddingLeft:2} : {paddingLeft:0}
      ]}>
        <Image
          style={{flex: 1, width: undefined, height: undefined}}
          source={image}
        />
      </View>
    );
  });
};

export default RenderSectionOne;

const styles = StyleSheet.create({});
