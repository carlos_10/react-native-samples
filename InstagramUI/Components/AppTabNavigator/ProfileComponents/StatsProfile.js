import {StyleSheet, Text, View} from 'react-native';

import React from 'react';

const StatsProfile = (props) => {
  return (
    <View style={{alignItems: 'center'}}>
      <Text>{props.number}</Text>
      <Text style={styles.textStyle}>{props.text}</Text>
    </View>
  );
};

export default StatsProfile;

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 10,
    color: 'grey',
  },
});
