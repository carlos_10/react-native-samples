import { Button, Icon } from "native-base";

import React from 'react';

const IconProfileButton = (props) => {
    return (
      <Button 
      transparent
      onPress={props.onPress}
      active={props.active}
      >
        <Icon name={props.name} style={props.style}/>
      </Button>
    );
  };

export default IconProfileButton;