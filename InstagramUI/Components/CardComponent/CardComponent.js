import {Body, Button, Card, CardItem, Icon, Left, Thumbnail} from 'native-base';
import {Image, StyleSheet, Text} from 'react-native';
import React, {useState} from 'react';

const CardComponent = (props) => {
  // const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";
  const images = {
    1: require('../../assets/img/feed/1.jpg'),
    2: require('../../assets/img/feed/2.jpg'),
    3: require('../../assets/img/feed/3.jpg'),
  };
  const [count, setCount] = useState(0);
  return (
    <Card>
      <CardItem>
        <Left>
          <Thumbnail large source={require('../../assets/img/me.jpg')} />
          <Body>
            <Text>Carlos Paredes</Text>
            <Text note>Sept 1, 2020</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem cardBody>
        <Image
          source={images[props.imageSource]}
          style={styles.imageContainer}
        />
      </CardItem>
      <CardItem style={{height: 45}}>
        <Left>
          <Button
            transparent
            onPress={() => {
              setCount(count + 1);
            }}>
            <Icon name="heart-outline" style={{color: 'black'}} />
          </Button>
          <Button transparent>
            <Icon name="chatbubbles-outline" style={{color: 'black'}} />
          </Button>
          <Button transparent>
            <Icon name="paper-plane-outline" style={{color: 'black'}} />
          </Button>
        </Left>
      </CardItem>
      <CardItem>
        <Text> {count} likes </Text>
      </CardItem>
      <CardItem>
        <Body>
          <Text>
            <Text style={{fontWeight: '700'}}>Carlos Paredes </Text>
            Eu nostrud ex eiusmod eiusmod aute laborum. Cillum dolor nulla Lorem
            quis minim voluptate magna sit aliquip amet deserunt mollit. Nulla
            eu nisi est occaecat esse nisi tempor aute et sit tempor Lorem anim.
            Aliqua nostrud ad dolore est nulla veniam et fugiat. Esse consequat
            eu ullamco eiusmod do voluptate eiusmod qui aliqua officia non Lorem
            incididunt esse.
          </Text>
        </Body>
      </CardItem>
    </Card>
  );
};

export default CardComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    flex: 1,
    height: 250,
    width: null,
  },
});
