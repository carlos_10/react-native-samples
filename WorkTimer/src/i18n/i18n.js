const  i18n = {
    TIME_FORMAT: 'HH:mm:ss',
    DATE_FORMAT: 'DD MMM YYYY',
    HOME: {
        WELCOME_HEADER: 'Hola Buenas!',
        FINISH: 'FINISH'
        
    },
    STOP_WATCH:{
        START: 'START',
        PAUSE: 'PAUSE',
    },
    FINISH:{
        FINISHED: 'You just Spent',
        ACTIVITYNAMECAP: 'Activity Name',
        CANCEL: 'Cancel',
        SAVE: 'Save',
    },
    HISTORY:{
        HISTORYCAP: 'HISTORY!!!' 
    }
}

export default i18n;