import React, {useState} from 'react';
import {SafeAreaView, Text, TouchableOpacity, View} from 'react-native';

import { ACTIVITY_STORAGE_KEY } from '../../config/consts';
import ActionButton from './ActionButton';
import AsyncStorage from '@react-native-community/async-storage';
import FinishViewStyle from './FinishViewStyle';
import ResponsiveCentered from './ResponsiveCentered';
import {TextInput} from 'react-native-gesture-handler';
import i18n from '../../i18n/i18n';
import moment from 'moment';

const FinishView = ({route, navigation}) => {
  const {timeSpent} = route.params;

  const [name, setName] = useState('');

  const saveTime = async () => {  
    let activities = await AsyncStorage.getItem(ACTIVITY_STORAGE_KEY);
    if (activities === null) {
      activities = [];
    } else {
      activities = JSON.parse(activities);
    }

    const date = new Date().getTime();

    activities.push({
      name,
      timeSpent,
      date,
    });

    await AsyncStorage.setItem(ACTIVITY_STORAGE_KEY, JSON.stringify(activities));
    navigation.goBack()
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 4, justifyContent: 'space-between'}}>
        <Text style={FinishViewStyle.mainHeader}>{i18n.FINISH.FINISHED}</Text>
        <Text style={FinishViewStyle.timerSubHeader}>
          {moment.utc(timeSpent).format(i18n.TIME_FORMAT)}{' '}
        </Text>
        <View style={{flex: 0.2}} />
      </View>
      <View style={{flex: 1}}>
        <ResponsiveCentered>
          <Text style={FinishViewStyle.activityNameLabel}>
            {i18n.FINISH.ACTIVITYNAMECAP}{' '}
          </Text>
        </ResponsiveCentered>
        <ResponsiveCentered>
          <TextInput
            style={FinishViewStyle.activityNameInput}
            value={name}
            onChangeText={txt => {
              setName(txt);
            }}
          />
        </ResponsiveCentered>
      </View>
      <View style={{flex: 5}}>
        <ResponsiveCentered>
          <View style={FinishViewStyle.actionButtonsContainer}>
            <ActionButton
              onPress={() => {
                navigation.goBack();
              }}
              label={i18n.FINISH.CANCEL}
              backgroundColor={'#F39527'}
              textColor={'#FFF'}
            />
            <ActionButton
            onPress={saveTime}
              label={i18n.FINISH.SAVE}
              backgroundColor={'#00CD5E'}
              textColor={'#FFF'}
            />
          </View>
        </ResponsiveCentered>
      </View>
    </SafeAreaView>
  );
};

export default FinishView;
