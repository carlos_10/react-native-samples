import React from 'react'
import { StyleSheet } from 'react-native'

const FinishViewStyle = StyleSheet.create({
     mainHeader: {
         fontSize:40,
          textAlign:'center',
          flex:1,
     },

     timerSubHeader: {
        fontSize:40,
        textAlign:'center',
        flex:1,
     },

     activityNameLabel:{
        fontSize:16,
        textAlign:'center',
     },

     activityNameInput:{
         borderRadius:5,
         borderColor:'#848484',
         borderWidth:1,
         height:44,
         marginTop:7,
     },

     buttonActionStyle:{
       width:134,
       height:44,
       borderRadius:15,  
     },
     
     actionButtonsContainer:{
        marginTop:65, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    }
});

export default FinishViewStyle


