import { Text, View } from 'react-native'

import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const ActionButtonStyles = StyleSheet.create({
    touchableStyles: {
        width:134,
        height:44,
        borderRadius:15,
        justifyContent:'center',
        alignItems:'center'
    },
    
    captionStyles:{
        fontSize:24,
        textTransform:'lowercase',        
    }
});

const ActionButton = ({label, textColor, backgroundColor, onPress}) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[ActionButtonStyles.touchableStyles, {backgroundColor}]}
        >
            <Text style={[ActionButtonStyles.captionStyles, {color: textColor}]}>{label}</Text>
        </TouchableOpacity>
    )
}

export default ActionButton


