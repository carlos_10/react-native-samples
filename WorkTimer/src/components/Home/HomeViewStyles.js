import {StyleSheet} from 'react-native';

const HomeViewStyles = StyleSheet.create({
  homeViewContainer: {
    alignItems: 'center',
  },

  weolcomeHeader: {
    marginTop: 50,
    textAlign: 'center',
    fontSize: 40,
    color: '#000',
  },

  finishiButtonText: {
    fontSize: 58,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: '#EA4C4C',
  },

  buttonsContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default HomeViewStyles;
