import { APPSTATETIMESTAMP_KEY, PAUSED_KEY, TIME_KEY } from '../../config/consts';
import {AppState, Text, TouchableOpacity, View} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import HomeViewStyles from './HomeViewStyles';
import React from 'react';
import StopWathcButton from '../StopWatchButton/StopWathcButton';
import i18n from '../../i18n/i18n';

class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0,
    };
    this.startTimmer = this.startTimmer.bind(this);
    this.pausedTimmer = this.pausedTimmer.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.handleAppStateChange('initial');
  }

  async handleAppStateChange(nextAppState) {
    console.log(nextAppState);
    const now = new Date().getTime();
    const {time, paused} = this.state;
    const readTime = parseInt(await AsyncStorage.getItem(TIME_KEY));
    const readStateTimestamp = parseInt(
      await AsyncStorage.getItem(APPSTATETIMESTAMP_KEY),
    );
    const timeDiff = now - readStateTimestamp;
    const newTime = readTime + timeDiff;

    if (!isNaN(readTime) && nextAppState === 'active' || nextAppState === 'initial') {
      const isPaused = await AsyncStorage.getItem(PAUSED_KEY);
      const wasPaused = isPaused && isPaused === 'true';
      let newState = {
        paused: wasPaused,
        time: readTime,
      };
      if (!wasPaused) {
        newState.time = newTime;
      }
      this.setState(newState, this.startTimmer);
    } else {
      await AsyncStorage.setItem(
        PAUSED_KEY,
        paused === true ? 'true' : 'false',
      );
      await AsyncStorage.setItem(TIME_KEY, time.toString());
      await AsyncStorage.setItem(APPSTATETIMESTAMP_KEY, now.toString());
    }
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  startTimmer() {
    this.clearTimmer();
    this.timerIntervalId = setInterval(() => {
      const {time, paused} = this.state;
      if (!paused) {
        // console.log('tic');
        this.setState({
          time: time + 1000,
        });
      }
    }, 1000);
  }

  clearTimmer(){
    if (this.timerIntervalId) {
      clearInterval(this.timerIntervalId);
    }
  }

  pausedTimmer() {
    const {paused} = this.state;
    this.setState({
      paused: !paused,
    });
  }

  renderFinishButton() {
    const {time, paused} = this.state;
    if (time && !paused) {
      return (
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate('Finish',{timeSpent: time});
          this.clearTimmer();
          this.setState({
            time:0,
          });
        }}>
          <Text style={HomeViewStyles.finishiButtonText}>
            {i18n.HOME.FINISH}
          </Text>
        </TouchableOpacity>
      );
    }
    return null;
  }

  render() {
    const {time, paused} = this.state;
    return (
      <View style={[{flex: 1}, HomeViewStyles.homeViewContainer]}>
        <View style={{flex: 1}}>
          <Text style={HomeViewStyles.weolcomeHeader}>
            {i18n.HOME.WELCOME_HEADER}
          </Text>
        </View>
        <View style={[{flex: 2}, HomeViewStyles.buttonsContainer]}>
          <StopWathcButton
            paused={paused}
            time={time}
            startOnPressedAction={this.startTimmer}
            timmerOnPressedAction={this.pausedTimmer}
          />
          {this.renderFinishButton()}
        </View>
      </View>
    );
  }
}

export default HomeView;
