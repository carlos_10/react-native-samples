import {StyleSheet, Text, View} from 'react-native';

import React from 'react';

const HistoryViewStyle = StyleSheet.create({
    historyPageHeader: {fontSize: 40, textAlign: 'center'},
  historyItemContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#EAEAEA',
    padding: 12,
    height: 68,
  },

  historyItemNameText: {
    fontSize: 18,
  },

  historyItemNameContainer: {
    flex: 2,
    alignItems: 'flex-end',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  historyTextDates: {fontSize: 14},
});
export default HistoryViewStyle;
