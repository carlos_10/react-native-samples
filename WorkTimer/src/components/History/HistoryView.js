import {FlatList, Text, View} from 'react-native';
import React, {Component} from 'react';

import { ACTIVITY_STORAGE_KEY } from '../../config/consts';
import AsyncStorage from '@react-native-community/async-storage';
import HistoryViewStyle from './HistoryViewStyle';
import {SafeAreaView} from 'react-native-safe-area-context';
import i18n from '../../i18n/i18n';
import moment from 'moment';

class HistoryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parsedActivities: [],
    };
    this.getActivities = this.getActivities.bind(this);
    props.navigation.addListener('focus', this.getActivities);
  }

  async getActivities() {
    const activities = await AsyncStorage.getItem(ACTIVITY_STORAGE_KEY);
    let parsedActivities = [];
    if (activities != null) {
      parsedActivities = JSON.parse(activities);
    }
    this.setState({parsedActivities: parsedActivities.reverse()}); //mostrarla revezmente
  }

  renderItem({item}) {
    return (
      <View style={HistoryViewStyle.historyItemContainer}>
        <View style={{flex: 4}}>
          <Text style={HistoryViewStyle.historyItemNameText}>{item.name}</Text>
        </View>
        <View style={HistoryViewStyle.historyItemNameContainer}>
          <View>
            <Text style={HistoryViewStyle.historyTextDates}>
              {moment.utc(item.date).format(i18n.DATE_FORMAT)}
            </Text>
          </View>
          <View>
            <Text style={HistoryViewStyle.historyTextDates}>
              {moment.utc(item.timeSpent).format(i18n.TIME_FORMAT)}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const {parsedActivities} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <Text style={HistoryViewStyle.historyPageHeader}>
          {i18n.HISTORY.HISTORYCAP}
        </Text>
        <FlatList
          data={parsedActivities}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => {
            return item.name + index;
          }}
        />
      </SafeAreaView>
    );
  }
}

export default HistoryView;
