import {Animated, Text, TouchableOpacity} from 'react-native';

import React from 'react';
import StopWathcButtonStyles from './StopWathcButtonStyles';
import i18n from '../../i18n/i18n';
import moment from 'moment';

const StopWathcButton = ({
  time,
  startOnPressedAction,
  timmerOnPressedAction,
  paused,
}) => {
  const timerOpacity = new Animated.Value(1);
  const BLINK_DELAY = 500;
  const blinker = (toValue) => {
    if (paused) {
      Animated.timing(timerOpacity, {
        toValue,
        duration: BLINK_DELAY,
      }).start(() => {
        blinker(toValue === 1 ? 0: 1);
      });
    } else {
      Animated.timing(timerOpacity, {
        toValue: 1,
        duration: BLINK_DELAY,
      }).start();
    }
  };
  
  blinker(0);

  if (time > 0) {
    return (
      <TouchableOpacity
        style={StopWathcButtonStyles.mainActionBottom}
        onPress={timmerOnPressedAction}>
        <Animated.View style={[StopWathcButtonStyles.mainActionBottom,{opacity: timerOpacity}]}>
          <Text style={StopWathcButtonStyles.mainActionBottomText}>
            {moment.utc(time).format(i18n.TIME_FORMAT)}
          </Text>
          <Text
            style={[
              StopWathcButtonStyles.mainActionBottomText,
              StopWathcButtonStyles.mainActionButtonPausedText,
            ]}>
            {i18n.STOP_WATCH.PAUSE}
          </Text>
        </Animated.View>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity
      style={StopWathcButtonStyles.mainActionBottom}
      onPress={startOnPressedAction}>
      <Text style={StopWathcButtonStyles.mainActionBottomText}>
        {i18n.STOP_WATCH.START}
      </Text>
    </TouchableOpacity>
  );
};

export default StopWathcButton;
