import {StyleSheet} from 'react-native';

const StopWathcButtonStyles = StyleSheet.create({
  mainActionBottom: {
    width: 264,
    height: 264,
    borderRadius: 142,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00CD5E',
  },

  mainActionBottomText: {
    fontSize: 58,
    color: '#FFF',
    fontWeight: 'bold',
  },

  mainActionButtonPausedText: {
    fontSize: 24,
  },
});

export default StopWathcButtonStyles;
