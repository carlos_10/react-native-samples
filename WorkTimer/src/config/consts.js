const ACTIVITY_STORAGE_KEY = '@activities';
const APPSTATETIMESTAMP_KEY = '@appStateTimestamp';
const TIME_KEY = '@time';
const PAUSED_KEY = '@isPaused';

export {ACTIVITY_STORAGE_KEY, APPSTATETIMESTAMP_KEY, TIME_KEY, PAUSED_KEY};
