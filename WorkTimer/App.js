/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import FinishView from './src/components/Finish/FinishView';
import HistoryView from './src/components/History/HistoryView';
import HomeView from './src/components/Home/HomeView'
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import {SafeAreaView} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <SafeAreaView style={{flex:1}}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Home' headerMode='none'>
          <Stack.Screen name='Home' component={BottomTabNavigator} />
          <Stack.Screen name='Finish' component={FinishView} />
        </Stack.Navigator>
        {/* <BottomTabNavigator/> */}
      </NavigationContainer>
    </SafeAreaView>
  );
};

const BottomTabNavigator = () => {
  return(    
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor:'#6D6D6D',
        inactiveTintColor: '#A3A3A3',
        labelStyle:{fontSize: 25, },
        style:{
          backgroundColor: '#E0E0E0',
          borderColor: 'rgba(140,140,140,0.8)',
          borderTopWidth: 1,          
        }        
      }}
    >
      <Tab.Screen name='Home' component={HomeView}/>      
      <Tab.Screen name='History' component={HistoryView } />
    </Tab.Navigator>
  );
}


export default App;
